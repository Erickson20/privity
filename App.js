/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */


import * as React from 'react';
import { Button,View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Views from "./view/index"






const Stack = createStackNavigator();
 function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        {Views.map((data,i)=>
          data.show&&(
            <Stack.Screen options={data.options} name={data.name} component={data.component} />
          )
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
}
export default App;
