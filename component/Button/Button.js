import React from "react"
import { Text,SafeAreaView, TouchableHighlight,View } from "react-native";
import Style from "./Button.style";


function Button({line = false,text,onPress}) {
  return (
    <View style={Style.content}>
      {!line?(
        <TouchableHighlight onPress={onPress}>
          <View style={Style.button}>
            <Text style={{color:'white', fontSize:20, fontWeight:'bold'}}>{text}</Text>
          </View>
        </TouchableHighlight>
      ):(
        <TouchableHighlight onPress={onPress}>
          <View style={Style.button2}>
            <Text style={{color:'#5863F8', fontSize:20, fontWeight:'bold'}}>{text}</Text>
          </View>
        </TouchableHighlight>
      )
      }

    </View>
  );
}

export default  Button
