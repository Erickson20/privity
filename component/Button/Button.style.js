import { StyleSheet } from "react-native";


const styles = StyleSheet.create({
  content:{
    padding: 10
  },
  button: {
    backgroundColor: "#5863F8",
    color:'white',
    width: 200,
    height: 50,
    borderRadius: 50,
    alignContent: 'center',
    alignItems: 'center',
    justifyContent:'center'
  },
  button2: {
    backgroundColor: "white",
    color:'#5863F8',
    width: 200,
    height: 50,
    borderRadius: 50,
    alignContent: 'center',
    alignItems: 'center',
    justifyContent:'center',
    borderWidth: 1,
    borderColor: '#5863F8'
  }

});

export default styles;
