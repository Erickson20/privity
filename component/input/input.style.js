import { StyleSheet } from "react-native";


const styles = StyleSheet.create({
  input: {
    height: 50,
    width: 350,
    padding: 20,
    margin:20,
    backgroundColor: '#F2F2F2',
    borderRadius: 50
  },

});

export default styles;
