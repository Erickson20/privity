import { Button, Text, View,TextInput,SafeAreaView } from "react-native";
import React from "react"
import Style from "./input.style"

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faCoffee } from '@fortawesome/free-solid-svg-icons'

function Input({onChange, value}) {

  return (
    <SafeAreaView >
      <TextInput
        style={Style.input}
        onChangeText={onChange}
        value={value}
      />
      <FontAwesomeIcon icon={ faCoffee } />
    </SafeAreaView>
  );
}

export default Input
