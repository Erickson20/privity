import { StyleSheet } from "react-native";


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  contentLogo:{
    flex: 4,
    backgroundColor: '#5863F8',

  },
  contentWave:{
    flex:1,
    backgroundColor:'white',
    justifyContent: 'flex-start',
  },
  contentBody:{
    flex:4,
    backgroundColor:'white',
    alignItems: 'center',
  },
  Textheadar:{
    fontSize: 25,
    fontWeight: 'bold',
    flexDirection: 'column',
    justifyContent:'center',
    alignItems: 'center',
  },
  TextDescription:{
    fontSize: 20,
    margin:20,
    flexDirection: 'column',
    justifyContent:'center',
    alignItems: 'center',
  }
});

export default styles;
