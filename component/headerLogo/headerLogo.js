import React from "react"
import { Text,SafeAreaView, View } from "react-native";
import Style from "./headerLogo.style";
import Svg, { Path } from "react-native-svg";


function HeaderLogo() {
  return (
    <SafeAreaView>
      <View style={Style.contentLogo}>

      </View>
      <View style={Style.contentWave}>

        <Svg
          height="100%"
          width="100%"
          viewBox="0 0 1440 310"
          style={{top:-5}}
        >
          <Path
            fill="#5863F8"
            fill-opacity="1" d="M0,0L120,53.3C240,107,480,213,720,218.7C960,224,1200,128,1320,80L1440,32L1440,0L1320,0C1200,0,960,0,720,0C480,0,240,0,120,0L0,0Z"></Path>
        </Svg>
      </View>
    </SafeAreaView>
  );
}

export default  HeaderLogo
