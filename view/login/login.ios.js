import { Text, View, TextInput, SafeAreaView,TouchableHighlight } from "react-native";
import React from "react"
import Svg, { Path } from 'react-native-svg';
import Style from "./style.ios"
import Input from "../../component/input/input"
import Button from "../../component/Button/Button"

function LoginIos({navigation}) {
  return (

    <View style={Style.container}>
      {/*Header==================================================*/}

      <View style={Style.contentLogo}>
      </View>
      <View style={Style.contentWave}>

        <Svg
          height="100%"
          width="100%"
          viewBox="0 0 1440 310"
          style={{top:-5}}
        >
          <Path
            fill="#5863F8"
            fill-opacity="1" d="M0,0L120,53.3C240,107,480,213,720,218.7C960,224,1200,128,1320,80L1440,32L1440,0L1320,0C1200,0,960,0,720,0C480,0,240,0,120,0L0,0Z"></Path>
        </Svg>
      </View>

      {/*Body==================================================*/}

      <View style={Style.contentBody}>
        <Text style={Style.Textheadar}>BIENVENIDO!</Text>
        <Text style={Style.TextDescription}>Favor introducir tu email</Text>
        <Input  onChange={()=>{}}/>
        <TouchableHighlight style={Style.link}>
          <Text style={{color:'#5863F8', fontWeight:'bold'}}>Forget you Password?</Text>
        </TouchableHighlight>
        <Button line={false} onPress={()=>{}} text={"LoginIos"}/>
        <Button line={true} onPress={()=>{}} text={"Registrate "}/>
      </View>

    </View>

  );
}





export default LoginIos
