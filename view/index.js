import Login from "./login/login";
import DetailsScreen from "./detail/detail";


const views = [
  {
    name: 'Login',
    show: true,
    component: Login,
    options:{
      headerShown:false
    }
  },
  {
    name: 'Detail',
    show: true,
    component: DetailsScreen,
    options:{
      headerShown:true
    }
  }
]
export default  views
